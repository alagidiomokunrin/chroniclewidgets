import 'package:chronicle_widgets/chronicle_widgets.dart';
import 'package:flutter/material.dart';

class ChronicleText extends StatelessWidget {
  final String text;
  final TextStyle textStyle;

  const ChronicleText({
    Key? key,
    required this.text,
    this.textStyle = const TextStyle(inherit: true),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: textStyle,
    );
  }

  static double _constrainTextStyleHeight(double fontSize) {
    return PageBackground.margin *
        (fontSize / PageBackground.margin).ceil() /
        fontSize;
  }
}

class ChronicleBodyText extends StatelessWidget {
  const ChronicleBodyText({
    Key? key,
    required this.text,
    this.textStyle,
  }) : super(key: key);
  final String text;
  final TextStyle? textStyle;

  @override
  Widget build(BuildContext context) {
    return ChronicleText(
      text: text,
      textStyle: textStyle?.copyWith(
              height: ChronicleText._constrainTextStyleHeight(
                  textStyle!.fontSize ?? defaultFont)) ??
          _globalTextStyle,
    );
  }

  static const double defaultFont = 12;

  static TextStyle _globalTextStyle = const TextStyle(
      fontSize: defaultFont, height: PageBackground.margin / defaultFont);

  ///Make sure the Textstyle's height fits into the margin
  static set globalTextStyle(TextStyle textStyle) {
    var fontSize = textStyle.fontSize ?? defaultFont;
    _globalTextStyle = textStyle.copyWith(
        height: ChronicleText._constrainTextStyleHeight(fontSize));
  }

  static TextStyle get globalTextStyle => _globalTextStyle;
}

class ChronicleHeaderText extends StatelessWidget {
  final String text;
  final TextStyle? textStyle;
  final bool isBold;
  final ChronicleHeadingNumber headerNumber;

  const ChronicleHeaderText({
    Key? key,
    required this.text,
    this.textStyle,
    this.isBold = true,
    required this.headerNumber,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChronicleText(
      text: text,
      textStyle: _getTextStyle(textStyle).copyWith(
          fontWeight: isBold ? FontWeight.bold : FontWeight.normal,
          height: ChronicleText._constrainTextStyleHeight(
              textStyle?.fontSize ?? _defaultFontSize(headerNumber))),
    );
  }

  static TextStyle _globalH1TextStyle = TextStyle(
      fontSize: _defaultFontSize(ChronicleHeadingNumber.h1),
      height:
          PageBackground.margin / _defaultFontSize(ChronicleHeadingNumber.h1));

  static TextStyle _globalH2TextStyle = TextStyle(
      fontSize: _defaultFontSize(ChronicleHeadingNumber.h2),
      height:
          PageBackground.margin / _defaultFontSize(ChronicleHeadingNumber.h2));

  static double _defaultFontSize(ChronicleHeadingNumber number) {
    switch (number) {
      case ChronicleHeadingNumber.h1:
        return 22;
      case ChronicleHeadingNumber.h2:
        return 18;
    }
  }

  TextStyle _getTextStyle(TextStyle? textStyle) {
    return textStyle ?? _getGlobalTextStyle(headerNumber);
  }

  TextStyle _getGlobalTextStyle(ChronicleHeadingNumber _headerNumber) {
    switch (headerNumber) {
      case ChronicleHeadingNumber.h1:
        return globalH1TextStyle;
      case ChronicleHeadingNumber.h2:
        return globalH2TextStyle;
    }
  }

  ///Make sure the Textstyle's height fits into the margin
  static set globalH1TextStyle(TextStyle textStyle) {
    var fontSize =
        textStyle.fontSize ?? _defaultFontSize(ChronicleHeadingNumber.h1);
    _globalH1TextStyle = textStyle.copyWith(
        height: ChronicleText._constrainTextStyleHeight(fontSize));
  }

  static TextStyle get globalH1TextStyle => _globalH1TextStyle;

  ///Make sure the Textstyle's height fits into the margin
  static set globalH2TextStyle(TextStyle textStyle) {
    var fontSize =
        textStyle.fontSize ?? _defaultFontSize(ChronicleHeadingNumber.h2);
    _globalH2TextStyle = textStyle.copyWith(
        height: ChronicleText._constrainTextStyleHeight(fontSize));
  }

  static TextStyle get globalH2TextStyle => _globalH2TextStyle;
}

enum ChronicleHeadingNumber {
  //Fontsize 22
  h1,
  //Fontsize 18
  h2,
}
