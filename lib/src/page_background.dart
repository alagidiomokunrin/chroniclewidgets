import 'package:flutter/material.dart';

class PageBackground extends StatelessWidget {
  const PageBackground({Key? key, @required this.child, this.shrinkWrap = true})
      : super(key: key);

  final Widget? child;
  final bool shrinkWrap;

  static const double margin = 18;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final margin = ((constraints.maxWidth % PageBackground.margin) +
                PageBackground.margin) /
            2.0;
        return SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: margin),
              child: ConstrainedBox(
                constraints: shrinkWrap
                    ? BoxConstraints.loose(
                        Size(constraints.maxWidth, constraints.maxHeight),
                      )
                    : BoxConstraints.expand(
                        height: constraints.maxHeight,
                        width: constraints.maxWidth,
                      ),
                child: CustomPaint(
                  painter: BulletJournalPainter(),
                  child: child,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class BulletJournalPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = Colors.black12
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    final end = size.bottomRight(Offset.zero);

    var start = size.topLeft(Offset.zero);

    while (start.dy <= end.dy) {
      while (start.dx <= end.dx) {
        canvas.drawCircle(start, 1.5, paint);
        start = start.translate(PageBackground.margin, 0);
      }
      start = start.translate(-start.dx, PageBackground.margin);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
