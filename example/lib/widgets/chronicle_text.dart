import 'package:chronicle_widgets/chronicle_widgets.dart';
import 'package:flutter/material.dart';

class ChronicleTextExample extends StatelessWidget {
  const ChronicleTextExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        Padding(
          padding: EdgeInsets.only(bottom: 18),
          child: ChronicleBodyText(
            text:
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 18),
          child: ChronicleHeaderText(
            text: "Heading 1 Bold",
            headerNumber: ChronicleHeadingNumber.h1,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 18),
          child: ChronicleHeaderText(
            text: "Heading 1",
            isBold: false,
            headerNumber: ChronicleHeadingNumber.h1,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 18),
          child: ChronicleHeaderText(
            text: "Heading 2 Bold",
            headerNumber: ChronicleHeadingNumber.h2,
          ),
        ),
        ChronicleHeaderText(
          text: "Heading 2",
          isBold: false,
          headerNumber: ChronicleHeadingNumber.h2,
        ),
      ],
    );
  }
}
